import os
import re

EXTENSIONES_TEXTO = ('txt', 'py', 'md', 'c')
MODOS = ('OR:', 'AND:', 'NOT:')   # En caso de querer hacer modificaciones en los modos de busqueda disponibles, es conveniente mantener estos mismos y en el mismo orden
TERMINAR = ('!QUIT')


########################## Funcion Principal #################################


def main():
    "Crea la interfaz para la interaccion con el usuario."
    print("Bienvenido a LiteSearch, un buscador simplificado para tu PC.\n\n")
    print("""
          Usted dispone de {} modos de busqueda:\n
          {} para saber en que rutas se encuentra alguno de los terminos.
          {} para saber en que rutas se encuentran todos los terminos.
          {} para saber en que ruta NO se encuentra un termino.\n\n\n""".format(
          len(MODOS), MODOS[0], MODOS[1], MODOS[2]))
    print("""El programa le permitira seguir buscando terminos hasta que ingrese {} o presione Cntrl+C.\n\n""".format(TERMINAR))

    indice = crear_indice_completo()
    buscar = []

    while not buscar or buscar[0] != TERMINAR:

        encontrado = []
        buscar = input('> ').upper().split()

        if not buscar or buscar[0] == TERMINAR:
            continue

        if buscar[0] not in MODOS or buscar[0] == MODOS[0]:
            if buscar[0] == MODOS[0]:
                 buscar.remove('OR:')
            busqueda_or(buscar, indice, encontrado)

        elif buscar[0] == MODOS[1]:
            busqueda_and(buscar[1:], indice, encontrado)

        else:
            if len(buscar) == 2:
                busqueda_not(buscar[1:], indice, encontrado)
            else:
                print('Por favor, realizar busquedas de un solo termino para el modo OR.')
                continue

        if not encontrado:
            print("No hay coincidencias.")

        for ruta in encontrado:
            print(ruta)


######################## Funciones de Busqueda ################################


def busqueda_or(terminos, indice, coincide):
    """
    Recibe una lista de terminos, un indice y una lista.

     Almacena en la lista las coincidencias de rutas en las que
     ALGUNO los terminos se encuentran."""

    for palabra in terminos:
        if not indice.get(palabra):
            continue

        for ruta in indice[palabra]:
            if ruta not in coincide:
                coincide.append(ruta)


def busqueda_and(terminos, indice, coincide):
    """
    Recibe una lista de terminos, un indice y una lista.

     Almacena en la lista las coincidencias de rutas en las que
     TODOS los terminos se encuentran.
    """

    for ruta in indice.get(terminos[0], []):
        coincide.append(ruta)

    for palabra in terminos[1:]:
        if not indice.get(palabra):
            coincide.clear()
            return

        for ruta in coincide:
            if ruta not in indice[palabra]:
                 coincide.remove(ruta)


def busqueda_not(termino, indice, coincide):
    """
    Recibe una lista de terminos, un indice y una lista.

     Unifica los terminos en uno solo y almacena en la lista las coincidencias
     de rutas en las que NO se encuentra el termino unificado.
    """

    termino = " ".join(termino)

    for clave in indice:
        if termino == clave:
            continue

        for ruta in indice[clave]:
            if ruta not in coincide and ruta not in indice.get(termino, []):
                coincide.append(ruta)


########################## Funciones de Indizado #################################


def crear_indice_completo():
    """
    Llama a todas las funciones de indizar y devuelve un indice unificado.

        Para todas las funciones 'indizar', las claves son
        terminos alfanumericos que identifican a cada nombre de archivo
        o directorio, en el caso de los archivos de texto tambien su contenido.
        Los valores son las rutas de cada archivo o directorio relativas
        a la direccion en la que se ejecuta el programa.
    """

    indice = {}
    ruta_abs = ''
    for origen, carpetas, contenido in os.walk('.'):
        for archivo in contenido:
            ruta_abs = os.path.join(origen, archivo)
            indizar_rutas_elementos(archivo, indice, ruta_abs)

            if re.split('\W+', archivo)[-1] not in EXTENSIONES_TEXTO:
                continue

            with open(ruta_abs) as leer:
                for linea in leer:
                     indizar_rutas_elementos(linea, indice, ruta_abs)

    return indice


def indizar_rutas_elementos(elemento, indice, ruta):
    """
    Agrega al indice cada clave perteneciente a cada subdirectorio
       o archivo reciba.
    """
    for clave in re.split('\W+', elemento):
        if not indice.get(clave.upper()):
            indice[clave.upper()] = []
        if ruta not in indice[clave.upper()]:
            indice[clave.upper()].append(ruta)


main()
